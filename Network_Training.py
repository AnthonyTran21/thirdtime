#Emotion Detector Code by: Anthony Tran and Abraham Adjo

import sys, os
import pandas as pd
import numpy as np
#Importing sys allows us to access important functions.
#Importing os allows us to interact with the operating system.
#Pandas is used for data analysis and manipulation
#Numpy is used for array computing, such as dot and cross products.
#The Keras model allows us to perform "experiments" on test data, which enables
#us to "train" the module enough for it to perform accurate enough emotion scans.

from keras.models import Sequential
#A sequential is a linear layer of stacks. These stacks are what we build the
#training module on.

from keras.layers import Dense, Dropout, Activation, Flatten
#The Dense class implements the most common and frequently used layer.
#The Dropout class allows the data to not be affected by unwanted data.
#Activation enables the layer to be used for rectified linear unit activation,
#a linear function that is the easiest to train.
#Flatten is used to convert the data into a one-dimensional array for input
#at the next layer.

from keras.layers import Conv2D, MaxPooling2D, BatchNormalization,AveragePooling2D
#Conv2D is a convolution kernal of two elements, height and width. It is used
#in matrix multiplication, to produce a third set of arrays from the Sequential.
#MaxPooling2D class allows the program to find the maximum value of each patch
#when we start creating a summary of features on a new layer;
#the process is called pooling.
#BatchNormalization is used to train the module, by standarizing the inputs to
#each mini-batch. The outcome? Reduced training batches and speeding up time.
#AveragePooling2D is similar to MaxPooling2D, instead it calculates the average
#of each patch on the feature map.

from keras.losses import categorical_crossentropy
#Categorical_crossentropy predicts the most probable match of each element of an
#array.

from keras.optimizers import Adam
#Adam is an optimizer that utilizes the Adam algorithm, which is useful for
#large data sets or parameters.

from keras.regularizers import l2
#l2 is a regularization class that is used for complex data patterns, such as
#emotion detection!

from keras.utils import np_utils
#Np_utils is another library with utilities such as .to_categorical.

df=pd.read_csv('fer2013.csv')
#Pandas is used to read the .csv file fer2013, which is an eXcel Comma Separated
#Values file that possesses 

X_train,train_y,X_test,test_y=[],[],[],[]
#This establishes the array list of four variables, as well as how it'll be
#organized

for index, row in df.iterrows():
    val=row['pixels'].split(" ")
    try:
        if 'Training' in row['Usage']:
           X_train.append(np.array(val,'float32'))
           train_y.append(row['emotion'])
        elif 'PublicTest' in row['Usage']:
           X_test.append(np.array(val,'float32'))
           test_y.append(row['emotion'])
    except:
        print(f"error occured at index :{index} and row:{row}")
#This for loop loops through the entire fer2013.csv, and fills in the array
#list using the format above. If it can't, or if there is missing data, it
#will print out an error.

num_features = 64
num_labels = 7
batch_size = 64
epochs = 30
width, height = 48, 48
#What these do is establish the parameters to be used in the pooling and
#categorical.


X_train = np.array(X_train,'float32')
train_y = np.array(train_y,'float32')
X_test = np.array(X_test,'float32')
test_y = np.array(test_y,'float32')
#Adds the values from the loop list into the array.

train_y=np_utils.to_categorical(train_y, num_classes=num_labels)
test_y=np_utils.to_categorical(test_y, num_classes=num_labels)
#These convert the class vectors integer of test_y and train_y to a binary 
#class matrix.

X_train -= np.mean(X_train, axis=0)
X_train /= np.std(X_train, axis=0)

X_test -= np.mean(X_test, axis=0)
X_test /= np.std(X_test, axis=0)
#These instructions normalize the data between 0 and 1.

X_train = X_train.reshape(X_train.shape[0], 48, 48, 1)

X_test = X_test.reshape(X_test.shape[0], 48, 48, 1)
#These instructions reshape the arrays to be fitted in the convolution layers.

model = Sequential()
#1st convolution layer: Emotion layer
model.add(Conv2D(64, kernel_size=(3, 3), activation='relu', input_shape=(X_train.shape[1:])))
model.add(Conv2D(64,kernel_size= (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2,2), strides=(2, 2)))
model.add(Dropout(0.5))

#2nd convolution layer: Pixel Layer
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2,2), strides=(2, 2)))
model.add(Dropout(0.5))

#3rd convolution layer: Usage layer.
model.add(Conv2D(128, (3, 3), activation='relu'))
model.add(Conv2D(128, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2,2), strides=(2, 2)))
#These instructions create the convolution layers that make up the model for testing. 
#Using Sequential, we create 3 layers, as the .csv file has three columns:
#Emotion, pixels, and usage.

model.add(Flatten())
#Flattens all three layers to be connected together.

#This adds the dimensions of the output neural node layers, along with
#the activation function which is used to define the output of these nodes.
model.add(Dense(1024, activation='relu'))
model.add(Dropout(0.2))
model.add(Dense(1024, activation='relu'))
model.add(Dropout(0.2))

#Another node activation, utilizing the number of labels for the dimensional
#output and a softmax activation function.
model.add(Dense(num_labels, activation='softmax'))

#Here we configure the module for training.
model.compile(loss=categorical_crossentropy,
              optimizer=Adam(),
              metrics=['accuracy'])

#We train the module here. The training is mainly calculating the emotional
#state of the person based on the pixels provided in the file. Epochs are
#iterations of a dataset, and in this case the number of iterations are 30.
model.fit(X_train, train_y,
          batch_size=batch_size,
          epochs=epochs,
          verbose=1,
          validation_data=(X_test, test_y),
          shuffle=True)


#We save the finished module as a .json file, as we write the finished module
#to the same folder as the directory. After this, we save the weighted value of
#the training file as well.
fer_json = model.to_json()
with open("fer.json", "w") as json_file:
    json_file.write(fer_json)
model.save_weights("fer.h5")