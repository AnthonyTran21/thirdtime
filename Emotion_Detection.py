#Emotion Detection 2 by: Anthony Tran and Abraham Adjo
import os
import cv2
import numpy as np
from keras.models import model_from_json
from keras.preprocessing import image
#Importing os allows us to interact with the operating system.
#Importing cv2 allows us to access image and video libraries.
#Numpy is used for array computing, such as dot and cross products.
#model_from_json allows us to open fer.json from Training.py.
#Importing image from keras.preprocessing allows us to use the functions located
#in image, such as img_to_array and img_pixels.

model = model_from_json(open("fer.json", "r").read())
#Here we load the model from Training.py.
model.load_weights('fer.h5')
#Here we load the weighted values that we saved as well.

face_haar_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
#Haar_cascade is a machine learning based approach where a cascade function
#is trained to detect objects in images (such as faces) from a lot of positive
#and negative images (from the Training.py .json file). the .xml file is
#provided in opencv/data/haarcascades as a pre-trained classifier, and the
#cv2.data.haarcascades is used to provide the full path to the .xml file, under
#the os library.

cap=cv2.VideoCapture(0)
#cap is our video capture. Since it is an external source, the parameter is 0.

while True:
    ret,test_img=cap.read()
    if not ret:
        continue
#If a frame is read, ret would return True, while test_img will return the next
#frame from the camera.

    gray_img= cv2.cvtColor(test_img, cv2.COLOR_BGR2GRAY)
#Here, we convert the frame from cv2's standard color scale of BGR to GRAY.
#Gray is used because in grayscale, the RGB values have equal intensity, which
#makes it simplier to record as opposed to full-color images with varying degrees
#of intensity of Red, Blue, and Green (RGB).

    faces_detected = face_haar_cascade.detectMultiScale(gray_img, 1.32, 5)
#detectMultiScale is used for face detection. We load in the gray converted
#image, while scaling the image to a factor of 1.32, with a minimum neighbor
#count of 5.

    for (x,y,w,h) in faces_detected:
        cv2.rectangle(test_img,(x,y),(x+w,y+h),(255,0,0),thickness=5)
        roi_gray=gray_img[y:y+w,x:x+h]
        roi_gray=cv2.resize(roi_gray,(48,48))
        img_pixels = image.img_to_array(roi_gray)
        img_pixels = np.expand_dims(img_pixels, axis = 0)
        img_pixels /= 255
#This for loop takes the detected face found and returns the positions of the
#face as a rectangle with paramaters(x direction, y direction, width, height).
#Once this is gathered, a region of interest (ROI) is created around the face area
#and is resized to a 48x48 square. Then, we convert the gray image to a Numpy
#array, expand it using np.expand_dims, and then divide it by 255, for use in
#predictions.

        predictions = model.predict(img_pixels)
#Predictions holds the predicted pixel array of arrays. It is an array of
#predicted array values between 0 and 1, which is accessed in the next line.

        max_index = np.argmax(predictions[0])
#This code finds the arrays that possess the maximum index value of the first element
#of the array. 

        emotions = ('Angry', 'Disgust', 'Fear', 'Happy', 'Sad', 'Surprise', 'Neutral')
        predicted_emotion = emotions[max_index]
#The first line creates the emotions that we can find based on the .csv file.
#As the emotion went from 0 to 6, there are 7 emotions.
#We use the max_index found in the predictions array, as the logic follows that
#if the ROI shows a lot of intensity in, say the mouth moving up for a smile,
#then the array index would reflect that in that region, the intensity is higher,
#which would be predicted as such from the neural network.

        cv2.putText(test_img, predicted_emotion, (int(x), int(y)), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255), 2)
#This places the predicted emotion being written on the box as RED, with the font being normal 
#size sans-serif at a select x and y value.

    resized_img = cv2.resize(test_img, (1000, 700))
    cv2.imshow('Facial emotion analysis ',resized_img)  
#The image is first resized then the image shown is the predicted emotion formatted
#above and then we format the emotion to be saved on the next line.

    if cv2.waitKey(10) == ord('q'):
        cv2.imwrite(emotions[max_index] + ".jpg", resized_img)
        break
#As the q button is pushed, we write the file to the directory and we close
#the capture.

cap.release()
cv2.destroyAllWindows()
#This is standard for cv2 to release the window and closes all windows.